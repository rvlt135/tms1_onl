number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
                10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
                14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
                17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}


def decorator(fun):
    def wrapped(num):
        number_names_index = {}
        numbers = num.split(" ")
        for key in numbers:
            number_names_index[int(key)] = number_names[int(key)]
        number_index = sorted(number_names_index.items(), key=lambda a: a[1])
        for value in number_index:
            fun(print(value[0]))
    return wrapped


@decorator
def number(*args):
    return args


number("1 2 3")
