from selenium import webdriver
import time


def test_login():
    url = "http://demo.guru99.com/test/newtours/register.php"
    path = "C:/chromedriver.exe"
    User_name = "Stas"
    Last_Name = "Herasimenko"
    Phone = "102"
    Email = "NotExisting@gmail.com"
    Address = "st.None, 1-1"
    City = "None"
    State = "None"
    PostalCode = "000000"
    Country = "BELARUS"
    Username = "GST"
    Password = "12345"
    Confirm_Password = "12345"
    driver = webdriver.Chrome(path)
    driver.get(url)
    Username_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/'
        'tr[5]/td/form/table/tbody/tr[2]/td[2]/input')
    Username_input.send_keys(User_name)
    Last_Name_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[3]/td[2]/input')
    Last_Name_input.send_keys(Last_Name)
    Phone_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[4]/td[2]/input')
    Phone_input.send_keys(Phone)
    Email_input = driver.find_element_by_xpath('//*[@id="userName"]')
    Email_input.send_keys(Email)
    Address_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[7]/td[2]/input')
    Address_input.send_keys(Address)
    City_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[8]/td[2]/input')
    City_input.send_keys(City)
    State_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[9]/td[2]/input')
    State_input.send_keys(State)
    PostalCode_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[10]/td[2]/input')
    PostalCode_input.send_keys(PostalCode)
    Country_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[11]/td[2]/select')
    Country_input.send_keys(Country)
    User_Name_input = driver.find_element_by_xpath('//*[@id="email"]')
    User_Name_input.send_keys(Username)
    Password_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[14]/td[2]/input')
    Password_input.send_keys(Password)
    Confirm_Password_input = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[15]/td[2]/input')
    Confirm_Password_input.send_keys(Confirm_Password)
    submit = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[5]/td/form/table/tbody/tr[17]/td/input')
    submit.click()
    time.sleep(1)
    registered_name = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[3]/td/p[1]/font/b').text
    registered_user_name = driver.find_element_by_xpath(
        '/html/body/div[2]/table/tbody/tr/td[2]/table/'
        'tbody/tr[4]/td/table/tbody/tr/td[2]/table/'
        'tbody/tr[3]/td/p[3]/font/b').text
    assert f"Dear {User_name} {Last_Name}," == registered_name
    assert f"Note: Your user name is {Username}." == registered_user_name
    driver.quit()
