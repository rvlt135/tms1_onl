# Задание 2
# Три варианта. Во втором окончание "ing"
# добавляется ко всем словам,
# кроме последнего (четвертого).
# Заменил последнее слово на пробел.
ending = "ing"
print(f"Clean{ending}, eat{ending}, go{ending} ")

msg2 = ['eat', 'go', 'clean', ' ']
print('ing, '.join(msg2))

print("Введите любое слово, "
      "\nно желательно на английском "
      "\nи желательно глагол")
string2 = input() + "ing"
print(string2)
