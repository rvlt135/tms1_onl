print("Добро пожаловать в игру 'Быки и коровы'/"
      "Загадано тайное 4-значное число. /"
      "Для победы вам необходимо угадать тайное число.")

print("Подсказка: Загадано число с неповторяющимися цифрами.")

is_playing = True
while is_playing:
    secret_number = "6732"
    Answer = input("Введите ваше число: ")
    true_number = "Вы выиграли!"
    false_number = "Попробуй еще!"
    if Answer == secret_number:
        print(true_number)
        is_playing = False
    elif Answer >= secret_number:
        print(false_number)
    elif Answer <= secret_number:
        print(false_number)
