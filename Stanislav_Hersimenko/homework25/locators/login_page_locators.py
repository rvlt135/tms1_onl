from selenium.webdriver.common.by import By


class LoginPageLocator:
    LOCATOR_EMAIL_LOGIN_FIELD = (By.ID, "email")
    LOCATOR_PASSWD_FIELD = (By.ID, "passwd")
    LOCATOR_SUBMIT_BUTTON = (By.ID, "SubmitLogin")
    LOCATOR_CREATE_ACCOUNT_TEXT = (
        By.XPATH, '//form[@id="create-account_form"]/h3')
