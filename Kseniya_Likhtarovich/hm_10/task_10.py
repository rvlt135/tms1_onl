"""
Создать несколько цветов. Собрать букет с определением его стоимости.

Определить время увядания по среднему времени жизни всех цветов.
Позволить сортировку цветов в букете на основе различных параметров.
Реализовать поиск цветов в букете по определенным параметрам.
Узнать, есть ли цветок в букете.
"""


class Flower:
    """Цветы для составления букета."""
    def __init__(self, name, color, length, days, price):
        self.name = name
        self.color = color
        self.length = length
        self.days = days
        self.price = price


class Accessory:
    """Декор для букета."""
    def __init__(self, name, price):
        self.name = name
        self.price = price


class Bouquet:
    """Создание букета."""
    def __init__(self):
        self.flowers = []
        self.accessories = []

    def add_flowers(self, *flowers):
        for flower in flowers:
            self.flowers.append(flower)

    def add_accessories(self, *accessories):
        for accessory in accessories:
            self.accessories.append(accessory)

    def calculate_price(self):
        flowers_price = sum([flower.price for flower in self.flowers])
        accessories_price = sum([accessory.price for accessory
                                 in self.accessories])
        price = flowers_price + accessories_price
        return f"Цена: {price} BYN"

    def determine_wilting_period(self):
        lifetime = sum([flower.days for flower in self.flowers])
        wilting_period = round(lifetime / len(self.flowers), 2)
        return f"Время увядания букета: {wilting_period} (сутки)"

    def sort_by_param(self, param):
        sorted_flowers = sorted(self.flowers, key=lambda x: getattr(x, param))
        flower_name = [flower.name for flower in sorted_flowers]
        return f"Сортировка цветов по параметру '{param}': {flower_name}"

    def find_by_param(self, param, value):
        search_result = [flower.name for flower in self.flowers
                         if getattr(flower, param) == value]
        return f"По параметру '{param}' и значению '{value}' " \
               f"найдено: {search_result}"

    def find_flower(self, name):
        flower_name = [flower.name for flower in self.flowers]
        if name in flower_name:
            return f"Цветок '{name}' есть в букете."
        else:
            return "Такого цветка нет в букете."


rose = Flower("Роза", "красный", 10, 2, 9)
chrysanthemum = Flower("Хризантема", "жёлтый", 8, 3, 7)
tulip = Flower("Тюльпан", "белый", 9, 2, 8)

wrapping_paper = Accessory("обёрточная бумага", 3)
ribbon = Accessory("лента", 1)

bouquet = Bouquet()

bouquet.add_flowers(rose, tulip, chrysanthemum)
bouquet.add_accessories(wrapping_paper, ribbon)

print(bouquet.calculate_price())
print(bouquet.determine_wilting_period())
print(bouquet.sort_by_param("color"))
print(bouquet.sort_by_param("length"))
print(bouquet.find_by_param("price", 7))
print(bouquet.find_flower("Тюльпан"))
print(bouquet.find_flower("Гвоздика"))
