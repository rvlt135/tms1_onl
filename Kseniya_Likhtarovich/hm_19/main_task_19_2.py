def game(player1, player2):
    bulls, cows = 0, 0
    for i in zip(player1, player2):
        if i[0] == i[1]:
            bulls += 1
        elif i[0] in player2:
            cows += 1
    else:
        if player1 == player2:
            return f"Вы победитель по жизни! Быки: {bulls}, Коровы: {cows}"
    return f"Быки: {bulls}, Коровы: {cows}."
