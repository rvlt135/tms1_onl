"""Найти самую частую букву в тексте."""

a = input().lower()
b = [i for i in a if i.isalpha()]
b.sort()
print(max(b, key=b.count))
