from selenium.webdriver.common.by import By


class MainPageLocators:

    LOCATOR_SIGN_IN = (By.CLASS_NAME, "login")
    LOCATOR_CART_LINK = (By.CSS_SELECTOR, "a[title='View my shopping cart']")
