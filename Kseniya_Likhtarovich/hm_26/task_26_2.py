from Kseniya_Likhtarovich.hm_26.task_26_1 import db, cursor

# Напечатать номер, дату, кол-во для заказа, который продал продавец 5002
query1 = """SELECT ord_no, ord_date, purch_amt
         FROM orders WHERE salesman_id = 5002"""
cursor.execute(query1)
print(cursor.fetchall())

# Напечатать уникальные id продавца(salesman_id). Используйте distinct
query2 = """SELECT DISTINCT salesman_id FROM orders"""
cursor.execute(query2)
print(cursor.fetchall())

# Напечатать по порядку данные о дате заказа, id продавца, номер заказа, кол-во
query3 = """SELECT ord_date, salesman_id, ord_no, purch_amt
         FROM orders"""
cursor.execute(query3)
print(cursor.fetchall())

# Напечатать заказы между 70001 и 70007(используйте between, and)
query4 = """SELECT * FROM orders
         WHERE ord_no BETWEEN 70001 AND 70007"""
cursor.execute(query4)
print(cursor.fetchall())

if __name__ == "__main__":
    db.close()
