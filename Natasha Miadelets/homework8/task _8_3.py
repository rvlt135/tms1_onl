# сделала только для простого выражения с двумя значениями
row = input("Введите выражение с пробелами:")
row_list = row.split()
symbols = "- + / * **"


def decorator(func):
    def wrapper(row):
        numbers = []
        symbol = []
        for i in row_list:
            if i in symbols:
                symbol.append(i)
            else:
                numbers.append(i)
        if symbol[0] == "-":
            result = int(numbers[0]) - int(numbers[1])
            print(result)
        elif symbol[0] == "+":
            result = int(numbers[0]) + int(numbers[1])
            print(result)
        elif symbol[0] == "*":
            result = int(numbers[0]) * int(numbers[1])
            print(result)
        elif symbol[0] == "/":
            result = int(numbers[0]) / int(numbers[1])
            print(result)
        elif symbol[0] == "**":
            result = int(numbers[0]) ** int(numbers[1])
            print(result)

    return wrapper


@decorator
def operation(row):
    print(row)


operation(row)
