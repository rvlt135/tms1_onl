from abc import ABC, abstractmethod


class Methods(ABC):
    @abstractmethod
    def addition(self, args1, args2):
        raise NotImplementedError

    @abstractmethod
    def subtraction(self, args1, args2):
        raise NotImplementedError

    @abstractmethod
    def multiplication(self, args1, args2):
        raise NotImplementedError

    @abstractmethod
    def division(self, args1, args2):
        raise NotImplementedError


class CalculationAction(Methods):

    def addition(self, args1, args2):
        self.validation(args1, args2)
        return f"Сумма  {args1} + {args2} = {args1 + args2}"

    def subtraction(self, args1, args2):
        self.validation(args1, args2)
        return f"Разность  {args1} - {args2} = {args1 - args2}"

    def multiplication(self, args1, args2):
        self.validation(args1, args2)
        return f"Произведение  {args1} * {args2} = {args1 * args2}"

    def division(self, args1, args2):
        self.validation(args1, args2)
        return f"Частное {args1} / {args2} = {args1 / args2}"

    def validation(self, args1, args2):
        assert isinstance(args1, (int, float)), f"Неверный тип {args1}"
        assert isinstance(args2, (int, float)), f"Неверный тип {args2}"
