from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By


def test_wiki_search_word():
    url = 'https://en.wikipedia.org/'
    word = 'Python (programming language)'
    driver = webdriver.Chrome('./chromedriver 2')
    driver.get(url)
    search_field = driver.find_element(By.ID, 'searchInput')
    search_field.send_keys(word)
    search_field.submit()
    sleep(5)
    article_title = driver.find_element(By.ID, 'firstHeading').text
    assert word == article_title, f'{article_title} is not equal {word}'
    driver.quit()
