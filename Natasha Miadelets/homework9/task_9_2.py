class Invest:
    def __init__(self, sum_inv, term_inv, percent):
        self.sum_inv = sum_inv
        self.term_inv = term_inv
        self.percent = percent


class Bank:
    def deposit(self, invest):
        for month in range(invest.term_inv):
            invest.sum_inv += invest.sum_inv * (invest.percent / (12 * 100))
        return f'Сумма на счету в конце вклада {invest.sum_inv}'


# (сумма вклада, период в месяцах, процентная ставка)
vklad = Invest(1000, 12, 10)
bank = Bank()
print(bank.deposit(vklad))
