from pages.base_page import BasePage
from locators.Cart_Page_Locators import CartPageLocators


class CartPage(BasePage):

    def should_be_cart_page(self):
        your_cart_text = self.find_element(
            CartPageLocators.LOCATOR_YOURCART_TEXT).text
        assert your_cart_text == "Your shopping cart",\
            f'CREATE AN ACCOUNT not eq {your_cart_text}'

    def check_that_cart_empty(self):
        cart_empty_text = self.find_element(
            CartPageLocators.LOCATOR_CART_EMPTY_TEXT).text
        assert cart_empty_text == "Your shopping cart is empty.",\
            f'CREATE AN ACCOUNT not eq {cart_empty_text}'

    def check_that_item_added(self):
        item_added_text = self.find_element(
            CartPageLocators.LOCATOR_ITEM_IN_CART).text
        assert item_added_text == "Printed Dress",\
            f'Printed Dress not eq {item_added_text}'
