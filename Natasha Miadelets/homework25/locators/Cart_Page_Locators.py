from selenium.webdriver.common.by import By


class CartPageLocators:

    LOCATOR_YOURCART_TEXT = (By.XPATH, "//span[text()='Your shopping cart']")
    LOCATOR_CART_EMPTY_TEXT = (By.XPATH,
                               "//p[text()='Your shopping cart is empty.']")
    LOCATOR_ITEM_IN_CART = (By.XPATH, "//td[@class='cart_description']/p/a")
