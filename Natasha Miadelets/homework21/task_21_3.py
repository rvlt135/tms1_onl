import yaml
import json

with open('order.yaml', 'r') as f:
    template = yaml.safe_load(f)

# Напечатать номер заказа и адрес
print("Номер заказа:", template["invoice"])
print("Адрес отправки:", template["bill-to"]["address"])

# Напечать описание посылки, ее стоимость и кол-во
for elem in template['product']:
    quantity = elem['quantity']
    description = elem['description']
    price = elem['price']
    print(f'Количество - {quantity}, описание - {description}, цена - {price}')

# Конвертирую yaml файл в json
with open("create_json.json", "w") as json_file:
    json.dump(str(template), json_file)


# Создаю свой yaml файл

first_templain = ['Miadelets Natasha']
second_templain = ['Miadelets Anna']
yaml_file = {'file1': first_templain, 'file2': second_templain}

with open('new_order.yaml', 'w') as f:
    yaml.dump(yaml_file, f)
