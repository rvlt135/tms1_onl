from selenium import webdriver


url = "https://ultimateqa.com/complicated-page/"
browser = webdriver.Chrome("./chromedriver 2")
button_xpath = "//div[@class=\"et_pb_button_module_wrapper" \
               " et_pb_button_4_wrapper et_pb_button_alignment_left" \
               " et_pb_module \"]/a"
button_css = ".et_pb_button.et_pb_button_4.et_pb_bg_layout_light"
button_class_name = "[class=\"et_pb_button et_pb_button_4" \
                    " et_pb_bg_layout_light\"]"
button_class_name1 = ".et_pb_button_module_wrapper.et_pb_button_4_wrapper." \
                     "et_pb_button_alignment_left.et_pb_module > :nth-child(1)"

browser.get(url)

button1 = browser.find_element_by_xpath(button_xpath)
button1.click()

button2 = browser.find_element_by_css_selector(button_css)
button2.click()

button3 = browser.find_element_by_css_selector(button_class_name)
button3.click()

button4 = browser.find_element_by_css_selector(button_class_name1)
button4.click()
browser.quit()
