from collections import Counter
a = "Hello, Ivan Ivanou! Welcome to Minsk"
b = "".join(c for c in a if c.isalnum())
c = Counter(b).most_common(1)
print(c)
