a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
ab = {**a, **b}

for i in ab.keys():
    ab[i] = [a.get(i), b.get(i)]
print(ab)
