from collections import Counter

lst = Counter([1, 5, 2, 9, 2, 9, 1])  # Высчитываем колличества повторений
dct = dict(lst)  # Преобразовываем в словарь(ключ:значение)
print(lst)

for key, value in dct.items():
    if dct[key] == 1:  # Знаем что уникальный ключ должен быть со значением 1
        print(key)
