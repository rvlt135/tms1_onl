alphabet_en = {'en_l': 'abcdefghijklmnopqrstuvwxy'
                       'zabcdefghijklmnopqrstuvwxyz',
               'en_h': 'ABCDEFGHIJKLMNOPQRSTUVWXYZA'
                       'BCDEFGHIJKLMNOPQRSTUVWXYZ'}
alphabet_ru = {'ru_l': 'абвгдеёжзийклмнопрстуфхцчшщъы'
                       'ьэюяабвгдеёжзийклмнопрстуфхцчшщъыьэюя',
               'ru_h': 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬ'
                       'ЭЮЯАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'}


def check_register(char):
    if char in alphabet_en["en_l"]:
        alphabet = alphabet_en["en_l"]
    elif char in alphabet_en["en_h"]:
        alphabet = alphabet_en["en_h"]
    elif char in alphabet_ru["ru_l"]:
        alphabet = alphabet_ru["ru_l"]
    else:
        alphabet = alphabet_ru["ru_h"]
    return alphabet


def encode(word, step):
    result = ""
    for i in word:
        alphabet = check_register(i)
        position = alphabet.find(i)
        new_position = position + step
        if i in alphabet:
            result += alphabet[new_position]
        else:
            result += i
    return result


def decode(word, step):
    result = ""
    for i in word:
        alphabet = check_register(i)
        position = alphabet.find(i)
        new_position = position - step
        if i in alphabet:
            result += alphabet[new_position]
        else:
            result += i
    return result


def check_cesar(word, step):
    encode_result = encode(word, step)
    print(f"Word: {word} >>>> Encode: {encode_result} "
          f">>>> Decode: {decode(encode_result, step)}")


check_cesar("Hello World!", 3)
check_cesar("this is, a, test. string", 2)
check_cesar("My NAME is SlIm Shady", 3)
check_cesar("Это стРока", 2)
check_cesar("Зайка моя, Я твой Зайчик", 2)
