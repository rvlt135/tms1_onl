class Investment:
    def __init__(self, money, years, percent):
        self.money = money
        self.years = years
        self.percent = percent
        self.result = 0


class Bank:
    def deposit(self, investment):
        action_1 = (1 + (investment.percent / 100) / 12)
        action_2 = (investment.years * 12)
        action = action_1 ** action_2
        investment.result = investment.money * action
        return investment.result


user_1 = Investment(100000, 3, 10)
bank_1 = Bank()
print(bank_1.deposit(user_1))
