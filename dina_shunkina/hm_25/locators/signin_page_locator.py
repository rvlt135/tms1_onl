from selenium.webdriver.common.by import By


class SigninPageLocator:
    LOCATOR_AUTHENTICATION_TEXT = (By.CSS_SELECTOR, ".page-heading")
    LOCATOR_ALREADY_REGISTERED_TEXT = (
        By.XPATH, "//h3[text()='Already registered?']")
    LOCATOR_EMAIL_FIELD = (By.ID, "email")
    LOCATOR_PASSWD_FIELD = (By.ID, "passwd")
    LOCATOR_SIGNIN_BUTTON = (By.CSS_SELECTOR, ".icon-lock.left")
    LOCATOR_ERROR = (By.CSS_SELECTOR, ".alert.alert-danger")
