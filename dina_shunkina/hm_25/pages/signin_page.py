from dina_shunkina.hm_25.pages.base_page import BasePage
from dina_shunkina.hm_25.locators.signin_page_locator import SigninPageLocator


class SigninPage(BasePage):

    def should_be_sing_page(self):
        self.visibility_element(SigninPageLocator.LOCATOR_AUTHENTICATION_TEXT)
        self.visibility_element(
            SigninPageLocator.LOCATOR_ALREADY_REGISTERED_TEXT)

    def sing_page(self, email: str, passwd: str):
        email_field = self.find_element(SigninPageLocator.LOCATOR_EMAIL_FIELD)
        email_field.send_keys(email)
        passwd_field = self.find_element(
            SigninPageLocator.LOCATOR_PASSWD_FIELD)
        passwd_field.send_keys(passwd)
        self.find_element(SigninPageLocator.LOCATOR_SIGNIN_BUTTON).click()

    def check_error(self):
        self.visibility_element(SigninPageLocator.LOCATOR_ERROR)
