"""Напечатайте уникальные id продавца(salesman_id).
Используйте distinct"""

import mysql.connector as mysql

db = mysql.connect(
    host="localhost",
    user="root",
    passwd="",
    database="task_26"
)
cursor = db.cursor()
query = "SELECT DISTINCT salesman_id FROM orders"
cursor.execute(query)
print(cursor.fetchall())
