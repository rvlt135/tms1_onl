from nikita_tyushkevich.homework25.pages.main_page import MainPage
from nikita_tyushkevich.homework25.pages.login_page import LoginPage


def test_main_login(driver):
    main_page = MainPage(driver)
    main_page.open_main_page()
    main_page.is_main_page()
    main_page.login_page()
    login_pages = LoginPage(driver)
    login_pages.should_be_login()
    login_pages.login("jd@domain.com", "Jetbalance")
