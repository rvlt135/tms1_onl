from nikita_tyushkevich.homework25.pages.base_page import BasePage
from nikita_tyushkevich.homework25.locators.basket_page_locators\
    import BasketPageLocators


class BasketPage(BasePage):

    def is_basket_page(self):
        summary = self.find_element(BasketPageLocators.LOCATOR_IS_BASKET_PAGE)
        assert summary.text == "SHOPPING-CART SUMMARY"

    def empty_basket(self):
        message = self.find_element(BasketPageLocators.LOCATOR_EMPTY_BASKET)
        assert message.text == "Your shopping cart is empty."
