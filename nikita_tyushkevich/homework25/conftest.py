import pytest
from selenium import webdriver


@pytest.fixture()
def driver():
    browser = webdriver.Chrome('./chromedriver.exe')
    browser.implicitly_wait(10)
    yield browser
    browser.quit()
