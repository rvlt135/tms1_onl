import pytest
from main_bullsncows import inp_check


@pytest.fixture
def get_answer():
    answer = 8465
    return str(answer)


class TestCases:

    @pytest.mark.smoke
    @pytest.mark.daily
    @pytest.mark.positive
    @pytest.mark.parametrize('param', ['8465'])
    def test_correct(self, param, get_answer):
        print("\n" f"Answer: {get_answer}")
        assert inp_check(param), "Should be 8465"

    @pytest.mark.daily
    @pytest.mark.positive
    def test_bulls_cows(self, get_answer):
        ret_value = (1, 1)
        print("\n" f"Answer: {get_answer}")
        assert inp_check('3264') == ret_value, f"Should be {ret_value}"

    @pytest.mark.daily
    @pytest.mark.positive
    def test_bulls(self, get_answer):
        ret_value = (0, 1)
        print("\n" f"Answer: {get_answer}")
        assert inp_check('3261') == ret_value, f"Should be {ret_value}"

    @pytest.mark.daily
    @pytest.mark.positive
    def test_bulls2(self, get_answer):
        ret_value = (0, 2)
        print("\n" f"Answer: {get_answer}")
        assert inp_check('3265') == ret_value, f"Should be {ret_value}"

    @pytest.mark.smoke
    @pytest.mark.daily
    @pytest.mark.positive
    def test_bulls3(self, get_answer):
        ret_value = (0, 3)
        print("\n" f"Answer: {get_answer}")
        assert inp_check('3465') == ret_value, f"Should be {ret_value}"

    @pytest.mark.daily
    @pytest.mark.positive
    def test_cows(self, get_answer):
        ret_value = (1, 0)
        print("\n" f"Answer: {get_answer}")
        assert inp_check('3218') == ret_value, f"Should be {ret_value}"

    @pytest.mark.daily
    @pytest.mark.positive
    def test_cows2(self, get_answer):
        ret_value = (2, 0)
        print("\n" f"Answer: {get_answer}")
        assert inp_check('4218') == ret_value, f"Should be {ret_value}"

    @pytest.mark.smoke
    @pytest.mark.daily
    @pytest.mark.positive
    def test_cows3(self, get_answer):
        ret_value = (3, 0)
        print("\n" f"Answer: {get_answer}")
        assert inp_check('4618') == ret_value, f"Should be {ret_value}"

    @pytest.mark.daily
    @pytest.mark.positive
    def test_cows4(self, get_answer):
        ret_value = (4, 0)
        print("\n" f"Answer: {get_answer}")
        assert inp_check('4658') == ret_value, f"Should be {ret_value}"

    @pytest.mark.smoke
    @pytest.mark.daily
    @pytest.mark.negative
    @pytest.mark.xfail(reason="Not unique values")
    def test_duplicated_values(self):
        assert inp_check('4465')

    @pytest.mark.smoke
    @pytest.mark.daily
    @pytest.mark.negative
    @pytest.mark.xfail(reason="Too large input")
    def test_too_large(self):
        assert inp_check('44651')

    @pytest.mark.smoke
    @pytest.mark.daily
    @pytest.mark.negative
    @pytest.mark.xfail(reason="Too short input")
    def test_too_short(self):
        assert inp_check('4')

    @pytest.mark.negative
    @pytest.mark.xfail(reason="Letters in input")
    def test_letters(self):
        assert inp_check('6aa1')

    @pytest.mark.negative
    @pytest.mark.xfail(reason="Special characters")
    def test_special_chars(self):
        assert inp_check('84$7')

    @pytest.mark.smoke
    @pytest.mark.daily
    @pytest.mark.negative
    @pytest.mark.xfail(reason="Empty input")
    def test_empty(self):
        assert inp_check('')
