import yaml
import json

from pprint import pprint
with open('order.yaml') as f:
    data = yaml.safe_load(f)

pprint(data)
print(f"Номер заказа: {data['invoice']}")
print(f"Адрес доставки: {data['bill-to']['address']}")
print()
print("Описание посылки, ее стоимость и кол-во:")
for item in data['product']:
    print(f"Продукт {item['sku']}")
    print(f"Описание: {item['description']}")
    print(f"Цена: {item['price']}")
    print(f"Кол-во: {item['quantity']}")
    print()

# Конвертация yaml файла в json
with open("order.json", "w") as json_file:
    json.dump(str(data), json_file)

# Создание нового yaml файла
product = 'dumbbells'
price = 20.99
to_yaml = {'product': product, 'price': price}
with open('order2.yaml', 'w') as f:
    yaml.dump(to_yaml, f)

with open('order2.yaml') as f:
    print(f.read())
