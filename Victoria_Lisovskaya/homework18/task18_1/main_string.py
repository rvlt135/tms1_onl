def counter(a: str):
    result = a[0]
    j = 0
    for i in range(len(a) - 1):
        if a[i] == a[i + 1]:
            j += 1
            # если последний элемент = предпоследнему, то выводим значение j
            if i == len(a) - 2:
                result += str(j + 1)
        else:
            if j > 0:
                result += str(j + 1)
                j = 0
            result += a[i + 1]
    return result
