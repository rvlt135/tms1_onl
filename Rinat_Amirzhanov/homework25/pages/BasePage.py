from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.home_page = "http://automationpractice.com/index.php"

    def open_main_page(self):
        self.driver.get(self.home_page)
        assert self.driver.title == "My Store"

    def find_element(self, locator: tuple, time=20):
        return WebDriverWait(self.driver, time).until(
            EC.presence_of_element_located(locator),
            message=f"Element can't be found by {locator}")
